#!/bin/env sh

# Dirty and quick hack to keep sbt triggered when using
# '~` commands. This significantly decreases the time
# I have to wait between restarts when I'm trying things.

# How to use: run sbt in one shell and this in a second shell.

trigger_file="$(realpath ./src/main/java/emptySbtTrigger.java)"
while true
do
    echo -n "//" > $trigger_file
    shuf -zer -n80 t r i g g e r e d >> $trigger_file
    sleep 1
done

