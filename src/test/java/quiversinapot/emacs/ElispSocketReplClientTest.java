/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.emacs;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ElispSocketReplClientTest {
    @Test
    public void testEval() {
        ElispSocketReplClient elisp = new ElispSocketReplClient();
        // TODO:
        //  1. prepare emacs
        //  2. check result
        // elisp.connect("localhost", 12345);
        String res = elisp.eval(
                "(with-current-buffer \"elisp-repl-server.el\""
                        + "'(\"just-testing\")"
                        + ")");
    }
}