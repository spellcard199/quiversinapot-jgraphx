/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot;

public class MainTest {

    public static void main(String[] args) throws InterruptedException {
        /*
         *  Using this for interactive development.
         */
        Main.main(new String[]{
                "--kawa-server-start",
                "--kawa-server-port=37146",
                "--ijava-start"}
        );
        Thread.sleep(999999999);
    }
}