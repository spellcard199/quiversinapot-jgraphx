/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.kawatodo;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;
import quiversinapot.globalstore.GlobalStore;

import java.io.File;
import java.io.FileInputStream;

public class KawaTodo {

    public static mxGraphComponent getStartupGraphComponent() {
        for (java.util.Map.Entry<mxGraphComponent, String> entry
                : GlobalStore.mxGraphComponents.entrySet()) {
            if (entry.getValue().equals("startup")) {
                return entry.getKey();
            }
        }
        return null;
    }

    public static mxGraph getStartupGraph() {
        for (java.util.Map.Entry<mxGraph, String> entry
                : GlobalStore.mxGraphs.entrySet()) {
            if (entry.getValue().equals("startup")) {
                return entry.getKey();
            }
        }
        return null;
    }

    public static void kawaInit() {
        String kawaInitPathStr = System.getProperty("user.home")
                + File.separator
                + ".quiversinapot/kawa-init.scm";

        File kawaInitFile = new File(kawaInitPathStr);
        if (kawaInitFile.exists()) {
            try {
                String kawaInitFileContents = new String(
                        new FileInputStream(kawaInitFile).readAllBytes());
                kawa.standard.Scheme.getInstance().eval(kawaInitFileContents);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
    }

}
