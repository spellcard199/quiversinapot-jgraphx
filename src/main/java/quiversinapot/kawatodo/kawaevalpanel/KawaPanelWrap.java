/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.kawatodo.kawaevalpanel;

import gnu.mapping.Location;
import kawa.standard.Scheme;
import kawadevutil.complete.Complete;
import kawadevutil.complete.CompletionDataForJava;
import kawadevutil.eval.Eval;
import kawadevutil.eval.EvalResultAndOutput;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.function.Supplier;

public class KawaPanelWrap {

    public KawaPanelWrap(Scheme scheme) {
        this.scheme = scheme;
    }

    // Fields

    private Scheme scheme;

    private static Font newFontWithSize(Font oldFont, String fontName, int newFontSize) {
        return new Font(
                fontName,
                oldFont.getStyle(),
                newFontSize);
    }

    private static Font newFontWithSize(Font oldFont, int newFontSize) {
        return new Font(
                oldFont.getFontName(),
                oldFont.getStyle(),
                newFontSize);
    }

    private JTextArea msgAreaNorth = ((Supplier<JTextArea>) () ->
    {
        JTextArea msgAreaNorth = new JTextArea();
        msgAreaNorth.setVisible(false);
        msgAreaNorth.setRows(0);
        msgAreaNorth.setEditable(false);
        msgAreaNorth.setFont(newFontWithSize(
                msgAreaNorth.getFont(), "Monospaced", 16));
        return msgAreaNorth;
    }).get();

    private JTextArea msgAreaSouth = ((Supplier<JTextArea>) () ->
    {
        JTextArea msgAreaSouth = new JTextArea();
        msgAreaSouth.setVisible(false);
        msgAreaSouth.setRows(0);
        msgAreaSouth.setEditable(false);
        msgAreaSouth.setFont(newFontWithSize(
                msgAreaSouth.getFont(), "Monospaced", 16));
        return msgAreaSouth;
    }).get();

    private JTextArea inputArea = ((Supplier<JTextArea>) () ->
    {
        JTextArea inputArea = new JTextArea();
        inputArea.setVisible(true);
        inputArea.setRows(0);
        inputArea.setFont(newFontWithSize(inputArea.getFont(), "Monospaced", 16));

        // Cancel
        inputArea.getActionMap().put(
                "kawa-cancel",
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        containerPanel.setVisible(false);
                        msgAreaNorth.setVisible(false);
                        msgAreaSouth.setVisible(false);
                    }
                }
        );
        inputArea.getInputMap().put(
                KeyStroke.getKeyStroke("control G"),
                "kawa-cancel"
        );

        // Eval
        inputArea.getActionMap().put(
                "kawa-eval",
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        EvalResultAndOutput evalResultAndOutput =
                                evaluator.evalCatchingOutErr(
                                        scheme,
                                        scheme.getEnvironment(),
                                        inputArea.getText());
                        String outErrAsString = evalResultAndOutput
                                .getOutErr()
                                .getOutAndErrInPrintOrder();
                        String resAsString =
                                evalResultAndOutput
                                        .getResultOfSupplier()
                                        .getResultAsString(true);
                        msgAreaNorth.setText(outErrAsString);
                        if (!resAsString.equals("")
                                && !outErrAsString.equals("")) {
                            msgAreaNorth.setText(msgAreaNorth.getText() + "\n");
                        }
                        msgAreaNorth.setText(msgAreaNorth.getText() + resAsString);

                        inputArea.setText("");
                        msgAreaNorth.setVisible(true);
                    }
                }
        );
        inputArea.getInputMap().put(
                KeyStroke.getKeyStroke("alt ENTER"),
                "kawa-eval"
        );

        // Font size
        inputArea.getActionMap().put(
                "increase-font-size",
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Font curFont = inputArea.getFont();
                        Font newFontMsgArea = newFontWithSize(
                                msgAreaNorth.getFont(),
                                msgAreaNorth.getFont().getSize() + 2);
                        Font newFontInputArea = newFontWithSize(
                                inputArea.getFont(),
                                inputArea.getFont().getSize() + 2);
                        msgAreaNorth.setFont(newFontInputArea);
                        inputArea.setFont(newFontInputArea);
                    }
                }
        );
        inputArea.getInputMap().put(
                KeyStroke.getKeyStroke("control PLUS"),
                "increase-font-size"
        );
        inputArea.getActionMap().put(
                "decrease-font-size",
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Font curFont = inputArea.getFont();
                        Font newFontMsgArea = newFontWithSize(
                                msgAreaNorth.getFont(),
                                msgAreaNorth.getFont().getSize() - 2);
                        Font newFontInputArea = newFontWithSize(
                                inputArea.getFont(),
                                inputArea.getFont().getSize() - 2);
                        msgAreaNorth.setFont(newFontInputArea);
                        inputArea.setFont(newFontInputArea);
                    }
                }
        );
        inputArea.getInputMap().put(
                KeyStroke.getKeyStroke("control MINUS"),
                "decrease-font-size"
        );

        // Completion for symbol
        inputArea.getActionMap().put(
                "kawa-complete-symbol",
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        // TODO: default kawa `ant' build includes neither:
                        //  - JLine dependency
                        //  - gnu/kawa/io/JLineInPort.java file
                        //  Make something equivalent to JLineInPort
                        //  but not dependent on JLine.
                        ArrayList<String> allComps = new ArrayList<>();
                        scheme.getLangEnvironment()
                                .enumerateAllLocations()
                                .forEachRemaining(
                                        (Location l) ->
                                                allComps.add(l.getKeySymbol().getName())
                                );
                        scheme.getEnvironment().enumerateLocations().forEachRemaining(
                                (Location l) ->
                                        allComps.add(l.getKeySymbol().getName())
                        );
                        msgAreaNorth.setText(String.join(" ", allComps));
                        msgAreaNorth.setVisible(true);
                        msgAreaNorth.setLineWrap(true);
                    }
                });
        inputArea.getInputMap().put(
                KeyStroke.getKeyStroke("alt I"),
                "kawa-complete-symbol"
        );


        // Completion for java
        inputArea.getActionMap().put(
                "kawa-complete-java",
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int pos = inputArea.getCaretPosition();
                        Optional<CompletionDataForJava> completionDataMaybe;
                        try {
                            completionDataMaybe = Complete.complete(
                                    inputArea.getText(),
                                    pos,
                                    getScheme(),
                                    getScheme().getEnvironment());
                            msgAreaNorth.setLineWrap(true);
                            if (completionDataMaybe.isPresent()) {
                                CompletionDataForJava completionData =
                                        completionDataMaybe.get();
                                String stringOfNames =
                                        String.join(" ",
                                                completionData.getNames());
                                msgAreaNorth.setText(stringOfNames);
                            }
                            msgAreaNorth.setVisible(true);
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }

                    }
                }
        );
        inputArea.getInputMap().put(
                KeyStroke.getKeyStroke("control alt I"),
                "kawa-complete-java"
        );

        return inputArea;
    }).get();

    JPanel containerPanel = ((Supplier<JPanel>) () ->
    {
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(msgAreaNorth, BorderLayout.NORTH);
        panel.add(inputArea, BorderLayout.SOUTH);
        return panel;
    }).get();

    private kawadevutil.eval.Eval evaluator = new kawadevutil.eval.Eval();

    // Getters

    public JPanel getContainerPanel() {
        return containerPanel;
    }

    public JTextArea getInputArea() {
        return inputArea;
    }

    public JTextArea getMsgAreaNorth() {
        return msgAreaNorth;
    }

    public Scheme getScheme() {
        return scheme;
    }

    public Eval getEvaluator() {
        return evaluator;
    }

    // Setters

    public void setContainerPanel(JPanel containerPanel) {
        this.containerPanel = containerPanel;
    }

    public void setScheme(Scheme scheme) {
        this.scheme = scheme;
    }

    public void setEvaluator(Eval evaluator) {
        this.evaluator = evaluator;
    }

    public void setInputArea(JTextArea inputArea) {
        this.inputArea = inputArea;
    }

    public void setMsgAreaNorth(JTextArea msgAreaNorth) {
        this.msgAreaNorth = msgAreaNorth;
    }
}
