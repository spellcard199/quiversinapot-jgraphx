/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.jgraphxqipdefaults.mouse.listeners.graphcomponent;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxPoint;
import com.mxgraph.view.mxGraphView;

import javax.swing.event.MouseInputListener;
import java.awt.event.MouseEvent;

public class MouseScrollHorizontal implements MouseInputListener {
    /*
     *  The reason we use 2 different types of listeners for
     *  horizontal and vertical scrolling is that
     *  (at least on my touchpad, on linux)
     *  Java's Swing sees double finger differently in the 2 dimensions:
     *  - vertical scrolling: as MouseWheelEvents
     *  - horizontal scrolling: as either
     *    - MouseEvent[MOUSE_PRESSED... ]
     *    - MouseEvent[MOUSE_RELEASED... ]
     *  As for horizontal scrolling the specific events fired
     *  are for mouse buttons 4 and 5. More precisely:
     *  - fingers moving towards right: Button4
     *  - fingers moving towards left: Button5
     *
     */

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getSource() instanceof mxGraphComponent.mxGraphControl
                && (e.getButton() == 4 || e.getButton() == 5)) {
            mxGraphComponent.mxGraphControl graphControl
                    = (mxGraphComponent.mxGraphControl) e.getSource();
            mxGraphComponent graphComponent = graphControl.getGraphContainer();
            mxGraphView view = graphComponent.getGraph().getView();
            mxPoint curTranslate = view.getTranslate();
            if (e.getButton() == 4) {
                view.setTranslate(
                        new mxPoint(
                                curTranslate.getX() + 20,
                                curTranslate.getY())
                );
            } else if (e.getButton() == 5) {
                view.setTranslate(
                        new mxPoint(
                                curTranslate.getX() - 30,
                                curTranslate.getY())
                );
            }
            // Without e.consume(), when the pointer passed over
            // an editable label it would enter editing mode.
            // Read comment for mouseReleased (below) for more details.
            // e.consume();
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // Without e.consume(), when the pointer passed over
        // an editable label it would enter editing mode.
        // If you read installDoubleClickHandler in mxGraphComponent
        // you can see that it enters editing when all are true:
        // - event is not consumed
        // - event is not null
        // - e.getClickCount() == 2
        // So it makes no difference between:
        // - mouse presses and releases
        // - mouse button number
        if (e.getButton() == 4 || e.getButton() == 5) {
            e.consume();
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

}
