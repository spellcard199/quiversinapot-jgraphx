/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.jgraphxqipdefaults.mouse.listeners.graphcomponent;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraphView;

import javax.swing.*;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

public class StatusBarOnWheel implements MouseWheelListener
{
    private JLabel statusBar;

    public StatusBarOnWheel(JLabel statusBar) {
        this.statusBar = statusBar;
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        if (e.getComponent() instanceof mxGraphComponent) {
            mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
            mxGraphView view = graphComponent.getGraph().getView();
            statusBar.setText("scale: " + (100 * view.getScale()) + "%");
        }
    }
}
