/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.jgraphxqipdefaults.mouse.listeners.graphcomponent;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxPoint;
import com.mxgraph.view.mxGraphView;
import kawadevutil.reflect.Reflecting;
import quiversinapot.coordinate.Geometry;

import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.lang.reflect.Field;

public class MouseScrollZoom implements MouseWheelListener {

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {

        if (e.getComponent() instanceof mxGraphComponent
                && e.isControlDown()) {
            mxGraphComponent graphComponent = (mxGraphComponent) e.getComponent();

            mxGraphView view = graphComponent.getGraph().getView();

            mxPoint mousePosBeforeGC = Geometry.viewCoordToGraphCoord(
                    new mxPoint(graphComponent.getMousePosition()),
                    view.getScale(),
                    view.getTranslate()
            );

            double oldScale = view.getScale();

            boolean zoomInLimitReached = view.getScale() * 100 > 500;
            boolean zoomOutLimitReached =
                    (view.getGraphBounds().getWidth()
                            / graphComponent.getGraphControl().getBounds().getWidth()
                    ) < 0.025;

            // e.getWheelRotation() < 0 : zoom-in
            // e.getWheelRotation() > 0 : zoom-out
            if ((e.getWheelRotation() < 0 && !zoomInLimitReached)
                    || (e.getWheelRotation() > 0 && !zoomOutLimitReached)
            ) {

                int inOut = e.getWheelRotation() > 0 ? -1 : 1;

                double newScale = oldScale * Math.pow(graphComponent.getZoomFactor(), inOut);

                mxPoint mousePosAfterGCEstimate =
                        Geometry.viewCoordToGraphCoord(
                                new mxPoint(e.getX(), e.getY()),
                                newScale,
                                view.getTranslate());

                double dx = mousePosAfterGCEstimate.getX() - mousePosBeforeGC.getX();
                double dy = mousePosAfterGCEstimate.getY() - mousePosBeforeGC.getY();

                double oldTranslateX = view.getTranslate().getX();
                double oldTranslateY = view.getTranslate().getY();
                double newTranslateX = oldTranslateX + dx;
                double newTranslateY = oldTranslateY + dy;

                /* Old version. Slightly slower.
                view.scaleAndTranslate(
                        newScale,
                        view.getTranslate().getX() + dx,
                        view.getTranslate().getY() + dy);
                // Compared to this code, what's below does not do validation,
                // so maybe it's slightly faster. See if this causes issues.
                */
                if (oldScale != newScale
                        || newTranslateX != oldTranslateX
                        || newTranslateY != newTranslateY) ;
                {
                    Reflecting r = Reflecting.getDefaultInstance();
                    try {
                        Field scaleField = r.getDeclaredField(view, "scale");
                        scaleField.setAccessible(true);
                        Field translateField = r.getDeclaredField(view, "translate");
                        translateField.setAccessible(true);
                        scaleField.set(view, newScale);
                        translateField.set(
                                view,
                                new mxPoint(
                                        newTranslateX,
                                        newTranslateY
                                ));
                    } catch (NoSuchFieldException | IllegalAccessException e1) {
                        e1.printStackTrace();
                    }
                }

                graphComponent.refresh();
            }
        }
    }
}
