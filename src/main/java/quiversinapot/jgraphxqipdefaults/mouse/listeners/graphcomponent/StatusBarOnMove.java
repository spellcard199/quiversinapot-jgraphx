/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.jgraphxqipdefaults.mouse.listeners.graphcomponent;

import com.mxgraph.model.mxCell;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class StatusBarOnMove implements MouseMotionListener {

    JLabel statusBar;

    public StatusBarOnMove(JLabel statusBar) {
        this.statusBar = statusBar;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (e.getSource() instanceof mxGraphComponent.mxGraphControl) {
            mxGraphComponent graphComponent = ((mxGraphComponent.mxGraphControl) e.getSource())
                    .getGraphContainer();
            mxGraph graph = graphComponent.getGraph();
            int cellCount = ((mxCell) graph.getDefaultParent()).getChildCount();
            statusBar.setText(
                    "[" + cellCount + "] " +
                            e.getX() + ", " + e.getY());
        } else {
            statusBar.setText(
                    e.getX() + ", " + e.getY());

        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        mouseDragged(e);
    }
};
