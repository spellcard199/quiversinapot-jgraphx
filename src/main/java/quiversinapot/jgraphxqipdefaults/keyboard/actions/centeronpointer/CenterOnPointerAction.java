/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.jgraphxqipdefaults.keyboard.actions.centeronpointer;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxPoint;
import com.mxgraph.view.mxGraphView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class CenterOnPointerAction extends AbstractAction {

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() instanceof mxGraphComponent) {

            mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
            Dimension s = graphComponent.getSize();
            double centerY = s.getHeight() / 2;
            double centerX = s.getWidth() / 2;

            Point mousePoint = MouseInfo.getPointerInfo().getLocation();
            // Mutate mousePoint in place: makes it relative to graphComponent
            SwingUtilities.convertPointFromScreen(mousePoint, graphComponent);

            mxGraphView view = graphComponent.getGraph().getView();

            // The smaller the scale, the greater the displacement in graph's coordinates.
            double dxTranslate = 1 / view.getScale() * (centerX - mousePoint.getX());
            double dyTranslate = 1 / view.getScale() * (centerY - mousePoint.getY());

            double newXTranslate = dxTranslate + view.getTranslate().getX();
            double newYTranslate = dyTranslate + view.getTranslate().getY();

            view.setTranslate(new mxPoint(newXTranslate, newYTranslate));
            graphComponent.refresh();

        }

    }
}
