/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.jgraphxqipdefaults.keyboard.actions.grab;

import com.mxgraph.model.mxCell;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxPoint;
import com.mxgraph.view.mxGraph;
import com.mxgraph.view.mxGraphView;
import quiversinapot.coordinate.Geometry;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class GrabMouseMotionListener implements MouseMotionListener {

    private mxPoint mouseStartPosInGraphCoords;
    private mxPoint prevMousePosInGraphCoords;
    // We need a reference to graphComponent because:
    // 1. mxEvent is fired by mxGraphComponent.mxGraphControl
    // 2. We need a mxGraph for graph operations
    // 3. We need a mxGraphComponent to get a mxGraph
    // 4. There isn't a way to get mxGraphComponent from mxGraphControl
    private mxGraphComponent graphComponent;

    // Used to cancel previous translations.
    private mxCell referenceCell;
    private double referenceCellStartPosX;
    private double referenceCellStartPosY;

    private boolean lockX = false;
    private boolean lockY = false;

    public GrabMouseMotionListener(mxGraphComponent graphComponent) {
        this.graphComponent = graphComponent;
        if (0 == graphComponent.getGraph().getSelectionCells().length) {
            // Nothing to do.
            cancelAndUndoGrabbing();
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {
        Object source = e.getSource();
        if (source instanceof mxGraphComponent.mxGraphControl) {
            mxGraph graph = this.graphComponent.getGraph();
            mxGraphView view = graph.getView();
            Object[] selectionCells = graph.getSelectionCells();
            if (referenceCell == null &&
                    selectionCells.length > 0) {
                referenceCell = (mxCell) selectionCells[0];
                referenceCellStartPosX = referenceCell.getGeometry().getX();
                referenceCellStartPosY = referenceCell.getGeometry().getY();
            }
            if (this.mouseStartPosInGraphCoords == null
                    || this.prevMousePosInGraphCoords == null) {
                this.mouseStartPosInGraphCoords = Geometry
                        .viewCoordToGraphCoord(
                                new mxPoint(e.getPoint()),
                                view.getScale(),
                                view.getTranslate()
                        );
                this.prevMousePosInGraphCoords = this.mouseStartPosInGraphCoords;
            }
            mxPoint mousePosInGraph = Geometry.viewCoordToGraphCoord(
                    new mxPoint(e.getPoint()),
                    view.getScale(),
                    view.getTranslate()
            );

            double eventDeltaX =
                    mousePosInGraph.getX() - this.prevMousePosInGraphCoords.getX();
            double eventDeltaY =
                    mousePosInGraph.getY() - this.prevMousePosInGraphCoords.getY();
            if (lockX) {
                eventDeltaY = 0;
            } else if (lockY) {
                eventDeltaX = 0;
            }

            translateSelectedVertices(graph, eventDeltaX, eventDeltaY);

            this.prevMousePosInGraphCoords = mousePosInGraph;
            this.graphComponent.refresh();
        }
    }

    public void translateSelectedVertices(mxGraph graph,
                                          double dxGraphCoords,
                                          double dyGraphCoords) {
        Object[] selected = graph.getSelectionCells();
        for (Object s : selected) {
            if (s instanceof mxCell) {
                mxCell cell = (mxCell) s;
                cell.getGeometry().translate(
                        dxGraphCoords, dyGraphCoords
                );
            }
        }
    }

    public void setLockX() {
        if (lockX) {
            lockX = false;
        } else {
            lockX = true;
            lockY = false;
            // "Un-do" previous translations along Y axis
            if (mouseStartPosInGraphCoords != null) {
                double deltaY = getTotalDeltaY();
                translateSelectedVertices(
                        graphComponent.getGraph(),
                        0,
                        -deltaY);
            }
        }
    }

    public void setLockY() {
        if (lockY) {
            lockY = false;
        } else {
            lockX = false;
            lockY = true;
            // "Un-do" previous translations along X axis
            if (mouseStartPosInGraphCoords != null) {
                double deltaX = getTotalDeltaX();
                translateSelectedVertices(
                        graphComponent.getGraph(),
                        -deltaX,
                        0);
            }
        }
    }

    public double getTotalDeltaX() {
        return referenceCell != null
                ? referenceCell.getGeometry().getX() - referenceCellStartPosX
                : 0;
    }

    public double getTotalDeltaY() {
        return referenceCell != null
                ? referenceCell.getGeometry().getY() - referenceCellStartPosY
                : 0;
    }

    public void cancelAndUndoGrabbing() {
        // Undo previous translations
        translateSelectedVertices(
                this.graphComponent.getGraph(),
                -getTotalDeltaX(),
                -getTotalDeltaY());
        GrabAction.ungrab(this.graphComponent);
    }
}
