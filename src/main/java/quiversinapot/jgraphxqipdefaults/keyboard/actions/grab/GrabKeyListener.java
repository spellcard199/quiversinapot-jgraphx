/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.jgraphxqipdefaults.keyboard.actions.grab;

import com.mxgraph.swing.mxGraphComponent;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

public class GrabKeyListener implements KeyListener {
    /*
     * TODO : listen for numbers for translation
     */

    GrabMouseMotionListener mouseMotionListener;
    private List<KeyStroke> ungrabWhen;

    public GrabKeyListener(GrabMouseMotionListener mouseMotionListener,
                           List<KeyStroke> ungrabWhen) {
        this.mouseMotionListener = mouseMotionListener;
        this.ungrabWhen = ungrabWhen;
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getSource() instanceof mxGraphComponent) {
            mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
            char eventChar = e.getKeyChar();
            int eventCode = e.getKeyCode();
            for (KeyStroke ks : ungrabWhen) {
                if (ks.getKeyChar() == eventChar) {
                    GrabAction.ungrab(graphComponent);
                }
            }
            if (eventChar == 'x') {
                mouseMotionListener.setLockX();
            } else if (eventChar == 'y') {
                mouseMotionListener.setLockY();
            } else if (eventCode == 27) {
                // 27 is for Escape
                mouseMotionListener.cancelAndUndoGrabbing();
            } else if (eventCode == 45) {
                // 45 is for minus
                // TODO
            } else if (48 <= eventCode && eventCode <= 57) {
                // 48: 0
                // 57: 9
                // TODO
            }
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}
