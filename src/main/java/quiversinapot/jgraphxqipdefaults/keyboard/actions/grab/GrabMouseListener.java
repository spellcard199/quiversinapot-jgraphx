/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.jgraphxqipdefaults.keyboard.actions.grab;

import com.mxgraph.swing.mxGraphComponent;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class GrabMouseListener implements MouseListener {

    // Event is fired by mxGraphComponent.mxGraphControl and:
    // 1. We need component for graph operations
    // 2. There isn't a way to get mxGraphComponent from mxGraphControl
    private mxGraphComponent graphComponent;

    public GrabMouseListener(mxGraphComponent graphComponent) {
        this.graphComponent = graphComponent;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        Object source = e.getSource();
        if (source instanceof mxGraphComponent.mxGraphControl) {
            GrabAction.ungrab(graphComponent);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
