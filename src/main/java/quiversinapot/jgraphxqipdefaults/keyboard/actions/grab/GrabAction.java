/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.jgraphxqipdefaults.keyboard.actions.grab;

import com.mxgraph.swing.mxGraphComponent;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.List;

public class GrabAction extends AbstractAction {

    // Note: every time the action is triggered a new object with sub-objects is created.
    //
    // These fields should not change after Instantiation.
    List<KeyStroke> ungrabKeyStrokes;

    // These fields are set every time GrabAction is triggered.
    // mxPoint mouseStartPosInGraphCoords;

    public GrabAction(List<KeyStroke> ungrabKeyStrokes) {
        this.ungrabKeyStrokes = ungrabKeyStrokes;
    }

    @Override
    public void
    actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source instanceof mxGraphComponent &&
                0 < ((mxGraphComponent) source).getGraph().getSelectionCells().length) {
            mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
            mxGraphComponent.mxGraphControl graphControl = graphComponent.getGraphControl();

            // Grab or stop grabbing.
            for (MouseMotionListener mml : graphControl.getMouseMotionListeners()) {
                if (mml instanceof GrabMouseMotionListener) {
                    ungrab(graphComponent);
                    return;
                }
            }

            GrabMouseMotionListener mouseMotionListener
                    = new GrabMouseMotionListener(graphComponent);
            GrabMouseListener mouseListener
                    = new GrabMouseListener(graphComponent);
            KeyListener keyListener
                    = new GrabKeyListener(mouseMotionListener, this.ungrabKeyStrokes);
            graphComponent.getGraphControl().addMouseMotionListener(mouseMotionListener);
            graphComponent.getGraphControl().addMouseListener(mouseListener);
            graphComponent.addKeyListener(keyListener);
        }

    }

    public static void
    ungrab(mxGraphComponent graphComponent) {
        mxGraphComponent.mxGraphControl
                graphControl = graphComponent.getGraphControl();
        // Remove grab listeners
        for (MouseMotionListener mml : graphControl.getMouseMotionListeners()) {
            if (mml instanceof GrabMouseMotionListener) {
                graphControl.removeMouseMotionListener(mml);
            }
        }
        for (MouseListener ml : graphControl.getMouseListeners()) {
            if (ml instanceof GrabMouseListener) {
                graphControl.removeMouseListener(ml);
            }
        }
        for (KeyListener kl : graphComponent.getKeyListeners()) {
            if (kl instanceof GrabKeyListener) {
                graphComponent.removeKeyListener(kl);
            }
        }
        graphComponent.refresh();
    }
}
