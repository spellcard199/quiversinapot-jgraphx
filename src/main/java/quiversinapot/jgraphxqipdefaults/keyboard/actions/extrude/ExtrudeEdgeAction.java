/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.jgraphxqipdefaults.keyboard.actions.extrude;

import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxICell;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;
import quiversinapot.jgraphxqipdefaults.keyboard.actions.grab.GrabMouseMotionListener;
import quiversinapot.globalstore.GlobalStore;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

public class ExtrudeEdgeAction extends AbstractAction {

    boolean reverseArrow;

    /*
    public ExtrudeEdgeAction(boolean reverseArrow) {
        this.reverseArrow = reverseArrow;
    }
    */

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof mxGraphComponent) {
            mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
            mxGraph graph = graphComponent.getGraph();
            ArrayList<mxCell> selectionCells = new ArrayList<>();
            for (Object cell : graph.getSelectionCells()) {
                selectionCells.add((mxCell) cell);
            }

            // Check if need to stop.
            if (graph.getSelectionCount() == 0) {
                return;
            } else {
                for (MouseMotionListener mml : graphComponent.getMouseMotionListeners()) {
                    if (mml instanceof GrabMouseMotionListener) {
                        return;
                    }
                }
            }

            Point mousePoint = MouseInfo.getPointerInfo().getLocation();
            // Mutate mousePoint in place
            SwingUtilities.convertPointFromScreen(mousePoint, graphComponent);
            mxCell cellUnderMouse = (mxCell) graphComponent.getCellAt(
                    (int) mousePoint.getX(),
                    (int) mousePoint.getY());

            ArrayList<mxICell> edgesExtruded = new ArrayList<>();
            if (cellUnderMouse != null) {
                for (mxCell scell : selectionCells) {
                    mxCell from = reverseArrow
                            ? cellUnderMouse
                            : scell;
                    mxCell to = reverseArrow
                            ? scell
                            : cellUnderMouse;
                    mxICell edge = (mxICell) graph.insertEdge(
                            graph.getDefaultParent(),
                            null, null, from, to);
                    edgesExtruded.add(edge);
                }
            }
            GlobalStore.lastlyExtrudedEdges = edgesExtruded;
        }
    }
}
