/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.jgraphxqipdefaults.keyboard.actions.extrude;

import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxICell;
import com.mxgraph.model.mxIGraphModel;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;
import quiversinapot.globalstore.GlobalStore;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class ReverseLastExtrudedEdges extends AbstractAction {

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof mxGraphComponent) {
            mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
            mxGraph graph = graphComponent.getGraph();
            mxIGraphModel model = graph.getModel();

            for (mxICell edge : GlobalStore.lastlyExtrudedEdges) {
                mxCell from = (mxCell) model.getTerminal(edge, true);
                mxCell to = (mxCell) model.getTerminal(edge, false);
                model.setTerminal(edge, from, false);
                model.setTerminal(edge, to, true);
            }
        }
    }
}
