/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.jgraphxqipdefaults.keyboard;

import com.mxgraph.swing.handler.mxKeyboardHandler;
import com.mxgraph.swing.mxGraphComponent;
import quiversinapot.jgraphxqipdefaults.keyboard.actions.centeronpointer.CenterOnPointerAction;
import quiversinapot.jgraphxqipdefaults.keyboard.actions.extrude.ExtrudeEdgeAction;
import quiversinapot.jgraphxqipdefaults.keyboard.actions.extrude.ReverseLastExtrudedEdges;
import quiversinapot.jgraphxqipdefaults.keyboard.actions.grab.GrabAction;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.util.List;

public class QipMxKeyboardHandler extends mxKeyboardHandler {
    // TODO (Maybe): if/when we need to switch to key
    //   sequences instead of keystrokes this should be
    //   replaced with a KeyListener that stores the last n events.

    public QipMxKeyboardHandler(mxGraphComponent graphComponent) {
        super(graphComponent);
    }

    @Override
    public InputMap getInputMap(int condition) {
        InputMap im = super.getInputMap(condition);
        // im.put(KeyStroke.getKeyStroke("G"), "grab");
        im.put(KeyStroke.getKeyStroke("G"), "grab");
        im.put(KeyStroke.getKeyStroke("E"), "extrudeEdge");
        im.put(KeyStroke.getKeyStroke("K"), "delete");
        im.put(KeyStroke.getKeyStroke("R"), "reverseLastExtrudedEdges");
        im.put(KeyStroke.getKeyStroke("F"), "centerOnPointer");
        return im;
    }

    @Override
    public ActionMap createActionMap() {
        ActionMap am = super.createActionMap();

        // Grab
        List<KeyStroke> ungrabKeysList = List.of(
                KeyStroke.getKeyStroke(KeyEvent.VK_G, 0, false),
                KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false)
                // KeyStroke.getKeyStroke("RETURN")
        );
        GrabAction grabAction = new GrabAction(ungrabKeysList);

        am.put("grab", grabAction);
        am.put("extrudeEdge", new ExtrudeEdgeAction());
        am.put("reverseLastExtrudedEdges", new ReverseLastExtrudedEdges());
        am.put("centerOnPointer", new CenterOnPointerAction());
        return am;
    }

}
