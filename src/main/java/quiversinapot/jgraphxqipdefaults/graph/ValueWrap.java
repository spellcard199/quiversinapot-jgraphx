/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.jgraphxqipdefaults.graph;

import org.w3c.dom.Element;

public class ValueWrap {

    /*
     *  Default user object class.
     *  The idea is that at serialization time (when user saves the diagram)
     *  the cell's value is replaced by the `value' field so that all other
     *  fields and methods implemented here disappear.
     *  The rationale is we can add more functionalities both here or
     *  extending this class via scripting or in a dependent project
     *  adding custom behavior (e.g. before or after setting `value'...)
     *  without making the final diagram less portable because it depended
     *  on a class not available on another system.
     *
     */

    // TODO:
    //  - add reactivity
    //  - can we add unwrapping of actual value at serialization time
    //    so that the fact we use a custom type gets abstracted away
    //    from the actual diagram?

    // Note: to create an element:
    // 1. Document doc = mxDomUtils.createDocument();
	// 2. Element value = doc.createElement(tagName);
    public Element value;
}
