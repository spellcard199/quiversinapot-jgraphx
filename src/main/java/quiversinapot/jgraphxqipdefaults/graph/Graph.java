/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.jgraphxqipdefaults.graph;

import com.mxgraph.model.mxCell;
import com.mxgraph.view.mxGraph;
import org.w3c.dom.Element;

import java.util.function.Function;

public class Graph {
    public static Function<Object, String> convertValueToString =
            new Function<>() {

                public mxGraph emptyMxGraph = new mxGraph();

                @Override
                public String apply(Object o) {
                    String valueStr;
                    if (o instanceof mxCell) {
                        mxCell cell = (mxCell) o;
                        Object value = cell.getValue();
                        if (value instanceof Element) {
                            Element valueElement = (Element) value;
                            if (valueElement.hasAttribute("label")) {
                                valueStr = valueElement.getAttribute("label");
                            } else {
                                valueStr = valueElement.toString();
                            }
                        } else if (value instanceof String) {
                            valueStr = (String) value;
                        } else {
                            valueStr = emptyMxGraph.convertValueToString(value);
                        }
                    } else {
                        valueStr = emptyMxGraph.convertValueToString(o);
                    }
                    return valueStr;
                }
            };
}
