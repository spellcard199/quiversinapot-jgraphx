/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot;

import com.mxgraph.model.mxCell;
import com.mxgraph.swing.handler.mxKeyboardHandler;
import com.mxgraph.swing.handler.mxRubberband;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.view.mxCellEditor;
import com.mxgraph.view.mxGraph;
import quiversinapot.globalstore.GlobalStore;
import quiversinapot.jgraphxqipdefaults.graph.Graph;
import quiversinapot.jgraphxqipdefaults.keyboard.QipMxKeyboardHandler;
import quiversinapot.jgraphxqipdefaults.keyboard.actions.showkawapanel.ShowKawaPanelAction;
import quiversinapot.jgraphxqipdefaults.mouse.listeners.graphcomponent.*;
import quiversinapot.kawatodo.kawaevalpanel.KawaPanelWrap;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;
import java.util.Map;

public class Startup {

    public static void startup() {
        mxGraph graph = newDefaultGraph();

        JLabel statusBar = newDefaultStatusBar();
        mxGraphComponent graphComponent = newDefaultGraphComponent(graph, statusBar);
        mxRubberband rubberband = new mxRubberband(graphComponent);
        mxKeyboardHandler keyboardHandler = new QipMxKeyboardHandler(graphComponent);
        // WIP vvv
        // Order matters: .setLayout() must go before .add(...)
        // graphPanel.setLayout(new BorderLayout());
        JPanel textPanel = new JPanel(new BorderLayout());
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(textPanel, BorderLayout.SOUTH);
        mainPanel.add(graphComponent);

        textPanel.add(statusBar, BorderLayout.NORTH);
        KawaPanelWrap kawaPanelWrap = new KawaPanelWrap(
                kawa.standard.Scheme.getInstance());
        textPanel.add(kawaPanelWrap.getContainerPanel(), BorderLayout.SOUTH);
        graphComponent.getActionMap().put(
                "showKawaPanel",
                new ShowKawaPanelAction(
                        kawaPanelWrap));
        graphComponent.getInputMap().put(
                KeyStroke.getKeyStroke("alt shift PERIOD"),
                "showKawaPanel");

        JFrame jframe = newDefaultJFrame(mainPanel);
        jframe.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);



        GlobalStore.mxGraphs.put(graph, "startup");
        GlobalStore.mxGraphComponents.put(graphComponent, "startup");
        GlobalStore.jframes.put(jframe, "startup");
        GlobalStore.keyboardHandlers.put(keyboardHandler, "startup");
        GlobalStore.rubberbands.put(rubberband, "startup");
    }

    public static mxGraph newDefaultGraph() {
        mxGraph graph = new mxGraph() {
            @Override
            public String convertValueToString(Object cell) {
                return Graph.convertValueToString.apply(cell);
            }
        };
        graph.setAutoSizeCells(false);
        {
            Map<String, Object> vertexStyle =
                    graph.getStylesheet().getDefaultVertexStyle();
            vertexStyle.put(
                    com.mxgraph.util.mxConstants.STYLE_FILLCOLOR,
                    "#FFFFFF"
            );
            vertexStyle.put(
                    com.mxgraph.util.mxConstants.STYLE_FONTFAMILY,
                    "Monospaced"
            );
            vertexStyle.put(
                    com.mxgraph.util.mxConstants.STYLE_FONTCOLOR,
                    "#000000"
            );
            vertexStyle.replace(
                    "align",
                    com.mxgraph.util.mxConstants.ALIGN_LEFT
            );
            // vertexStyle.replace(
            //         com.mxgraph.util.mxConstants.STYLE_STROKECOLOR,
            //         "C3D9FF"
            // );
            // vertexStyle.replace(
            //         com.mxgraph.util.mxConstants.STYLE_FONTCOLOR,
            //         "C3D9FF"
            // );
            // vertexStyle.remove(
            //         com.mxgraph.util.mxConstants.STYLE_FILLCOLOR
            // );
            for (Map.Entry<String, Object> e : vertexStyle.entrySet()) {
                System.out.println(e);
            }
        }

        // Map<String, Object> vertexStyle = graph.getStylesheet().getDefaultVertexStyle();
        // graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, mxConstants.NONE);
        // graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, "FFFFFF");
        System.out.println(
                graph.getStylesheet().getStyles()
        );
        // vertexStyle.put(mxConstants.STYLE_PERIMETER, mxConstants.NONE);
        mxCell parent = (mxCell) graph.getDefaultParent();
        graph.getModel().beginUpdate();

        /*
        graph.addListener(null, new mxEventSource.mxIEventListener() {
            @Override
            public void invoke(Object sender, mxEventObject evt) {
                System.out.println("mxIEventListener - Begin");
                System.out.println(sender);
                System.out.println(evt.getProperties().getOrDefault("cells", "toast"));
                System.out.println(System.currentTimeMillis());
                System.out.println("mxIEventListener - End");
            }
        });
        graph.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                System.out.println("PropertyChangeEvent - Begin");
                System.out.println(evt);
                System.out.println("PropertyChangeEvent - End");
            }
        });
        */

        try {
            mxCell v0 = (mxCell) graph.insertVertex(
                    parent, null, "Node 0", 0, 0, 80, 30
            );
        } finally {
            graph.getModel().endUpdate();
        }

        // ((mxCell) graph.getSelectionCell()).set

        graph.selectAll();
        return graph;
    }

    public static mxGraphComponent newDefaultGraphComponent(
            mxGraph graph, JLabel statusBar) {
        mxGraphComponent graphComponent = new mxGraphComponent(graph);
        graphComponent.setVerticalScrollBarPolicy(21);
        graphComponent.setHorizontalScrollBarPolicy(31);
        graphComponent.getViewport().setOpaque(true);  // optional?
        graphComponent.getViewport().setBackground(Color.BLACK);
        graphComponent.setZoomFactor(1.7);
        // Scroll to zoom
        graphComponent.addMouseWheelListener(
                new MouseScrollZoom()
        );
        // Scroll to move view
        /*
         *  Note that:
         *    - vertical scrolling:
         *      - comes from: mxGraphComponent
         *      - arrives as: MouseWheelEvent
         *    - horizontal scrolling
         *      - comes from: mxGraphComponent.mxGraphControl
         *      - arrives as: MouseEvent[MOUSE_PRESSED] or MouseEvent[MOUSE_RELEASED]
         *        - fingers moving right: Button4
         *        - fingers moving left : Button5
         */
        mxGraphComponent.mxGraphControl graphControl = graphComponent.getGraphControl();
        MouseListener[] mxDefaultMouseListeners
                = graphControl.getMouseListeners();
        for (MouseListener ml : mxDefaultMouseListeners) {
            graphControl.removeMouseListener(ml);
        }

        graphControl.addMouseListener(new SetCurrentGraphComponent());
        graphComponent.getGraphControl().addMouseListener(new MouseScrollHorizontal());
        graphComponent.addMouseWheelListener(new MouseScrollVertical());
        // Status bar info
        graphComponent.addMouseWheelListener(new StatusBarOnWheel(statusBar));
        graphComponent.getGraphControl().addMouseMotionListener(
                new StatusBarOnMove(statusBar)
        );

        // Restore default listeners we removed before, but after ours.
        for (MouseListener ml : mxDefaultMouseListeners) {
            graphControl.addMouseListener(ml);
        }
        System.out.println("--------------------------------------------");
        System.out.println("graphComponent.getGraphControl() listeners:");
        System.out.println(graphComponent.getGraphControl().getMouseListeners().length);
        for (MouseListener ml : graphComponent.getGraphControl().getMouseListeners()) {
            System.out.println(ml);
        }
        System.out.println("-end----------------------------------------");

        ((mxCellEditor) graphComponent.getCellEditor()).setShiftEnterSubmitsText(true);
        graphComponent.setGridColor(new Color(50, 100, 200));
        graphComponent.setGridVisible(true);

        return graphComponent;
    }

    public static JPanel newDefaultJPanel(mxGraphComponent graphComponent) {
        JPanel jPanel = new JPanel();
        // Order matters: .setLayout() must go before .add(...)
        jPanel.setLayout(new BorderLayout());
        jPanel.add(graphComponent);
        return jPanel;
    }

    public static JLabel newDefaultStatusBar() {
        JLabel statusBar = new JLabel();
        statusBar.setBorder(BorderFactory.createEmptyBorder(
                2, 2, 2, 2));
        return statusBar;
    }

    public static JFrame newDefaultJFrame(JPanel jpanel) {
        // This way we have control over decorations (I'm referring to Gnome).
        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame jFrame = new JFrame("Quiversinapot");
        jFrame.getContentPane().add(jpanel);
        jFrame.setSize(new Dimension(600, 400));
        jFrame.setLocationRelativeTo(null);
        // We don't need no decorations...
        jFrame.getRootPane().setWindowDecorationStyle(JRootPane.NONE);
        jFrame.setVisible(true);
        return jFrame;
    }
}
