/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.commandline;

import kawadevutil.kawa.KawaTelnetServerTrackingClients;
import kawageiser.StartKawaWithGeiserSupport;
import picocli.CommandLine;
import quiversinapot.jupyter.QipAlmond;
import quiversinapot.jupyter.QipIJava;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.concurrent.Callable;

@CommandLine.Command(name = "command-line-options")
public class StartupCommand implements Callable<Boolean> {

    private KawaTelnetServerTrackingClients kawaServerWrapper = null;

    public Optional<KawaTelnetServerTrackingClients>
    getKawaServerWrapper() {
        return kawaServerWrapper != null
                ? Optional.of(kawaServerWrapper)
                : Optional.empty();
    }

    @CommandLine.ArgGroup(exclusive = false)
    KawaServerParsedOpts kawaServerParsedOpts;
    static class KawaServerParsedOpts {
        @CommandLine.Option(names = "--kawa-server-port", required = true)
        int port;
        @CommandLine.Option(names = "--kawa-server-start", defaultValue = "false", required = true)
        boolean start = false;
    }

    @CommandLine.ArgGroup(exclusive = false)
    AlmondOpts almondOpts;
    static class AlmondOpts {
        @CommandLine.Option(names = "--almond-start", required = true)
        boolean start;
    }

    @CommandLine.ArgGroup(exclusive = false)
    IJavaKernel ijavaKernelOpts;
    static class IJavaKernel {
        @CommandLine.Option(names = "--ijava-start", required = true)
        boolean start;
    }

    @Override
    public Boolean call() throws Exception {

        if (kawaServerParsedOpts != null) {
            try {
                Path connFileDir = Paths.get(System.getProperty("user.home"))
                        .resolve(".quiversinapot");
                connFileDir.toFile().mkdirs();
                File connFile = connFileDir.resolve("qip-kawa-telnet-port.txt").toFile();

                try (FileOutputStream fos = new FileOutputStream(connFile)) {
                    fos.write(String.valueOf(kawaServerParsedOpts.port).getBytes());
                }

                this.kawaServerWrapper =
                        StartKawaWithGeiserSupport
                                .startKawaServerWithGeiserSupport(
                                        kawaServerParsedOpts.port
                                );
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }

        if (ijavaKernelOpts != null) {
            QipIJava.startIJavaKernel();
        }

        if (almondOpts != null) {
            QipAlmond.startScalaKernel();
        }

        return true;
    }
}
