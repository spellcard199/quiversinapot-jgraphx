/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.coordinate;

import com.mxgraph.util.mxPoint;

public class Geometry {

    public static mxPoint
    viewCoordToGraphCoord(mxPoint pointInViewCoord,
                          double scale,
                          mxPoint translate) {
        return new mxPoint(
                (pointInViewCoord.getX() / scale) - translate.getX(),
                (pointInViewCoord.getY() / scale) - translate.getY()
        );
    }

    public static mxPoint
    graphCoordToViewCoord(mxPoint pointInGraphCoord,
                          double scale,
                          mxPoint translate) {
        return new mxPoint(
                scale * (pointInGraphCoord.getX() + translate.getX()),
                scale * (pointInGraphCoord.getY() + translate.getY())
        );
    }

}
