/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.jupyter;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

public class QipIJava {

    public static void startIJavaKernel() {

        String key = GenerateRandom.generateRandomBase64Token(33);

        String connFileContents = "{\n" +
                "  \"shell_port\": 59285,\n" +
                "  \"iopub_port\": 49793,\n" +
                "  \"stdin_port\": 33841,\n" +
                "  \"control_port\": 48257,\n" +
                "  \"hb_port\": 35703,\n" +
                "  \"ip\": \"127.0.0.1\",\n" +
                "  \"key\": \"" + key + "\",\n" +
                "  \"transport\": \"tcp\",\n" +
                "  \"signature_scheme\": \"hmac-sha256\",\n" +
                "  \"kernel_name\": \"qui-ijava\"\n" +
                "}";

        try {
            Path connFileDir = Paths.get(System.getProperty("user.home"))
                    .resolve(".quiversinapot");
            connFileDir.toFile().mkdirs();
            File connFile = connFileDir.resolve("qip-ijava.json").toFile();

            try (FileOutputStream fos = new FileOutputStream(connFile)) {
                fos.write(connFileContents.getBytes());
            }
            System.out.println("IJava connection file:\n" + connFile.getAbsolutePath());

            new Thread(
                    () -> {
                        try {
                            io.github.spencerpark.ijava.IJava.main(
                                            new String[]{connFile.getAbsolutePath()}
                                    );
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
            ).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
