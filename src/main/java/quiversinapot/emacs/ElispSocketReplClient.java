/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.emacs;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ElispSocketReplClient {
    /*
     *  Client for the elisp socket repl listening in emacs
     *  TODO: add server file (elisp) in resources
     */

    /*
     * TODO : write client for custom json protocol
     * {
     *     error : "...",
     *     result: "...",
     *     output: "...",
     * }
     *
     * Q. Why a custom json protocol instead of emacs' json-rpc-server package?
     * A. Because to me passing plain elisp to emacs is more readable and for now
     *    I don't need all the other features json-rpc-server offers.
     *
     */

    java.net.Socket client;
    OutputStream out;
    InputStream in;

    public void connect(String host, int port) {
        try {
            client = new java.net.Socket(host, port);
            out = client.getOutputStream();
            in = client.getInputStream();
        } catch (IOException ex) {
            String logMsg = String.format(
                    "[ERROR] Failed to connect to host '%s' at port '%d'."
                    , host, port);
            Logger.getLogger(ElispSocketReplClient.class.getName())
                    .log(Level.SEVERE, logMsg, ex);
            ex.printStackTrace();
        }
    }

    public void connect(int port) {
        connect("localhost", port);
    }

    public String eval(String expr) {

        StringBuilder output = new StringBuilder();

        try {

            // String sending = expr.concat("\n");
            String sending = expr;
            out.write(sending.getBytes());

            // Wait for emacs to answer
            while (output.toString().isEmpty()
                    && in.available() == 0) {
                Thread.sleep(1);
            }

            // Get answer
            while (in.available() != 0) {
                output.append((char) in.read());
            }

        } catch (InterruptedException | IOException ex) {
            Logger.getLogger(ElispSocketReplClient.class.getName())
                    .log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }

        return output.toString().stripTrailing();
    }
}
