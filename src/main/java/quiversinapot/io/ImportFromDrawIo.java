/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.io;

import com.mxgraph.io.mxCodec;
import com.mxgraph.util.mxUtils;
import com.mxgraph.util.mxXmlUtils;
import com.mxgraph.view.mxGraph;
import org.w3c.dom.Document;

import java.io.IOException;
import java.util.Optional;

public class ImportFromDrawIo {

    public static Optional<mxGraph> importFromDrawIo(String file) {

        // TODO: doesn't work. Maybe draw.io has a diferent schema from JGraphX?

        Document doc = null;
        try {
            String contents = mxUtils.readFile(file);
            doc = mxXmlUtils.parseXml(contents);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mxCodec codec = new mxCodec(doc);
        mxGraph newGraph = new mxGraph();
        if (doc != null) {
            codec.decode(doc.getDocumentElement(), newGraph.getModel());
            return Optional.of(newGraph);
        } else {
            return Optional.empty();
        }
    }
}
