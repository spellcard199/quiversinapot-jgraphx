/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.io;

import com.mxgraph.io.mxCodec;
import com.mxgraph.util.mxXmlUtils;
import com.mxgraph.view.mxGraph;

import java.io.FileWriter;
import java.io.IOException;

public class SerializeToXml {

    public static void writeToXmlFile(mxGraph graph, String dest) {
        mxCodec codec = new mxCodec();
        String xmlStr = mxXmlUtils.getXml(codec.encode(graph.getModel()));
        try (FileWriter fw = new java.io.FileWriter(dest)) {
            fw.write(xmlStr);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
