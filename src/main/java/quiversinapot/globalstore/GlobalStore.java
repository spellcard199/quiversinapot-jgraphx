/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.globalstore;

import com.mxgraph.model.mxICell;
import com.mxgraph.swing.handler.mxKeyboardHandler;
import com.mxgraph.swing.handler.mxRubberband;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;
import kawadevutil.kawa.KawaTelnetServerTrackingClients;

import javax.swing.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class GlobalStore {
    // Just a place to store various things with global visibility

    public static mxGraphComponent currentGraphComponent = null;
    public static List<mxICell> lastlyExtrudedEdges = Collections.emptyList();
    public static HashMap<KawaTelnetServerTrackingClients, String>
            kawaserver = new HashMap<>();
    // TODO: instead of running respective mains, learn how to start both getting:
    //  - ijava kernel
    //  - almond kernel
    public static HashMap<JFrame, String> jframes = new HashMap<>();
    public static HashMap<mxGraphComponent, String> mxGraphComponents = new HashMap<>();
    public static HashMap<mxGraph, String> mxGraphs = new HashMap<>();
    public static HashMap<mxRubberband, String> rubberbands = new HashMap<>();
    public static HashMap<mxKeyboardHandler, String> keyboardHandlers = new HashMap<>();
    public static HashMap<Thread, String> threads = new HashMap<>();
}
