/*
 * Copyright 2020 spellcard199
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package quiversinapot.jupyter

import java.io.{File, FileOutputStream}
import java.nio.file.Path
import java.nio.file.Paths

object QipAlmond {

  def startScalaKernel() {

    val key: String = GenerateRandom.generateRandomBase64Token(33)

    val connFileContents: String =
      s"""
     |{
     |  "shell_port": 59286,
     |  "iopub_port": 49794,
     |  "stdin_port": 33842,
     |  "control_port": 48258,
     |  "hb_port": 35704,
     |  "ip": "127.0.0.1",
     |  "key": "$key",
     |  "transport": "tcp",
     |  "signature_scheme": "hmac-sha256",
     |  "kernel_name": "qip-almond"
     |}""".stripMargin('|').stripLeading

    val connFileDir: Path =
      Paths.get(System.getProperty("user.home")).resolve(".quiversinapot")
    connFileDir.toFile.mkdirs()
    val connFile: File = connFileDir.resolve("qip-almond.json").toFile

    val fos = new FileOutputStream(connFile)
    fos.write(connFileContents.getBytes())

    System.out.println("Almond connection file:\n" + connFile.getAbsolutePath)

    new Thread(
      () => almond.ScalaKernel.main(
        Array("--connection-file", connFile.getAbsolutePath))
    ).start()

  }
}
