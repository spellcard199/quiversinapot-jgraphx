(setq user-init-file (or load-file-name (buffer-file-name)))
(setq user-emacs-directory
      (concat (file-name-directory user-init-file)
              "emacs.d/"))

(require 'evil)
(require 'evil-escape)
(require 'evil-surround)
(require 'geiser-kawa)
(require 'jupyter)

;; Appearance
(setq visible-bell nil)
(menu-bar-mode -1)
(tool-bar-mode -1)

;; Fixes readibility problem due to background too bright
(setq shr-color-visible-luminance-min 100)
;; No tabs
(setq-default indent-tabs-mode nil)

;; Evil
(setq evil-escape-key-sequence "jk")
(evil-mode 1)
(evil-escape-mode 1)

;; Useful
(defun indent-buffer ()
  (interactive)
  (indent-region (point-min) (point-max)))

;; Kawa
(setq geiser-kawa-use-included-kawa t)

;; Quiversinapot
(defvar qip/user-dir (concat (expand-file-name "~")
                             "/.quiversinapot/"))
(defvar qip/kawa-telnet-file-name "qip-kawa-telnet-port.txt")
(defvar qip/ijava-conn-file-name "qip-ijava.json")
(defvar qip/almond-conn-file-name "qip-almond.json")

(defun qip/geiser-kawa-connect ()
  (interactive)
  (let* ((file-path (concat
                     qip/user-dir
                     qip/kawa-telnet-file-name))
         (telnet-port-as-string (with-temp-buffer
                                  (insert-file-contents file-path)
                                  (string-trim
                                   (buffer-substring-no-properties
                                    (point-min)
                                    (point-max)))))
         (telnet-port (string-to-number
                       telnet-port-as-string)))
    (geiser-connect
     'kawa "localhost" telnet-port)))

(defun qip/jupyter-connect-repl (ijava-or-almond)
  (cl-assert (member ijava-or-almond '(ijava almond)))
  (let (
        (conn-file-name (cond ((equal ijava ijava-or-almond)
                               "qip-ijava.json")
                              ((equal almond ijava-or-almond)
                               "qip-almond.json"))))
    (jupyter-connect-repl (concat qip-user-dir conn-file-name) )))

(defun qip/jupyter-connect-repl-ijava()
  (interactive)
  (jupyter-connect-repl (concat qip/user-dir
                                qip/ijava-conn-file-name)
                        nil nil nil t))

(defun qip/jupyter-connect-repl-almond()
  (interactive)
  (jupyter-connect-repl (concat qip/user-dir
                                qip/almond-conn-file-name)
                        nil nil nil t))

(provide 'qip-init)
