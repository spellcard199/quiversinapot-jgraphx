import sbt.Keys._
import sbtassembly.AssemblyPlugin.autoImport.PathList

name := "quiversinapot"

version := "0.1"

// scalaVersion := "2.12.10"    // ammonite not found with 2.12.10
scalaVersion := "2.13.1"

resolvers += "jitpack" at "https://jitpack.io"
libraryDependencies ++= Seq(
  "com.github.vlsi.mxgraph" % "jgraphx" % "4.0.5"
  , "info.picocli" % "picocli" % "4.1.4"
  , "com.gitlab.spellcard199" % "geiser-kawa" % "-SNAPSHOT"
  , "com.github.waikato.thirdparty" % "ijava" % "1.3.0"
  , "sh.almond" % "scala-kernel_2.13.1" % "0.9.1"

  , "org.testng" % "testng" % "7.0.0"
)

mainClass in(Compile, run) := Some("quiversinapot.Main")
mainClass in(Compile, packageBin) := Some("quiversinapot.Main")

// Without this some global variables become messed up.
fork := true

// we specify the name for our fat jar
assemblyJarName := "quiversinapot-assembly.jar"
assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs@_*) => MergeStrategy.discard
  case x => MergeStrategy.first
}

// Tasks for starting various servers. This part is temporary and
// just to save typing at the sbt console.
lazy val runWithKawaParams = Array(
  "--kawa-server-start",
  "--kawa-server-port=37146")
lazy val runWithKawa =
  taskKey[Unit]("A custom run task.")
fullRunTask(
  runWithKawa,
  Runtime,
  "quiversinapot.Main",
  runWithKawaParams: _*)

lazy val runWithKawaIJavaParams =
  Array(
    "--kawa-server-start",
    "--kawa-server-port=37146",
    "--ijava-start")
lazy val runWithKawaIJava =
  taskKey[Unit]("A custom run task.")
fullRunTask(
  runWithKawaIJava,
  Runtime,
  "quiversinapot.Main",
  runWithKawaIJavaParams: _*)

lazy val runWithKawaIJavaAlmondParams =
  Array(
    "--kawa-server-start",
    "--kawa-server-port=37146",
    "--ijava-start",
    "--almond-start")
lazy val runWithKawaIJavaAlmond =
  taskKey[Unit]("A custom run task.")
fullRunTask(runWithKawaIJavaAlmond,
  Runtime,
  "quiversinapot.Main",
  runWithKawaIJavaAlmondParams: _*)
